(function(L) {
    L.ExtendedDivIcon = L.DivIcon.extend({
        options: {
            //Options de base
            iconSize: [12, 12],
            html: false,
            bgPos: null,
            className: 'leaflet-div-icon',
            //Options etendues
            id: null,
            htmlId: null
        },
        // Surcharge de la création pour ajouter un id à la div
        createIcon: function(oldIcon) {
            var div = L.DivIcon.prototype.createIcon.call(this, oldIcon);

            if(this.options.id) {
              div.id = this.options.id;
            }

            if(this.options.style) {
              for(var key in this.options.style) {
                div.style[key] = this.options.style[key];
              }
            }

            return div;
        },
        animate: function() {
            var thisIcon = this;
            // TODO : utiliser 'L.DomUtil.get'
            setInterval(function(){
                setTimeout(function(){
                    $("#" + thisIcon.options.htmlId)[0].style.width = '50px'
                    $("#" + thisIcon.options.htmlId)[0].style.height = '50px'
                    $("#" + thisIcon.options.htmlId)[0].style.marginLeft = '-15px'
                    $("#" + thisIcon.options.htmlId)[0].style.marginTop = '-15px'
                }, 1000)

                setTimeout(function(){
                    $("#" + thisIcon.options.htmlId)[0].style.width = '20px'
                    $("#" + thisIcon.options.htmlId)[0].style.height = '20px'
                    $("#" + thisIcon.options.htmlId)[0].style.marginLeft = '0px'
                    $("#" + thisIcon.options.htmlId)[0].style.marginTop = '0px'
                }, 2000)
            }, 2000);
        },
        setColor: function(gasrate){
            var color = "";
            if(gasrate<25){
                color = "green";
            }else if(gasrate < 50){
                color = "yellow";
            }else if(gasrate < 75){
                color = "orange";
            }else if(gasrate < 101){
                color = "red";
            }
            
            var thisIcon = L.DomUtil.get(this.options.id);
            var thisInnerHtmlIcon = L.DomUtil.get(this.options.htmlId);
            
            L.DomUtil.removeClass(thisIcon, "green08");
            L.DomUtil.removeClass(thisIcon, "yellow08");
            L.DomUtil.removeClass(thisIcon, "orange08");
            L.DomUtil.removeClass(thisIcon, "red08");
            L.DomUtil.addClass(thisIcon, color + "08");
            
            L.DomUtil.removeClass(thisInnerHtmlIcon, "green03");
            L.DomUtil.removeClass(thisInnerHtmlIcon, "yellow03");
            L.DomUtil.removeClass(thisInnerHtmlIcon, "orange03");
            L.DomUtil.removeClass(thisInnerHtmlIcon, "red03");
            L.DomUtil.addClass(thisInnerHtmlIcon, color + "03");
        }
  });

  L.extendedDivIcon = function(options) {
    return new L.ExtendedDivIcon(options);
  }
})(window.L);

function createIcon(id){
    var icon = L.extendedDivIcon({
        id: "icon_" + id,
        htmlId: "html_icon_" + id,
        iconSize: null,
        iconAnchor: [10, 10],
        popupAnchor: [10, 0],
        shadowSize: [0, 0],
        className: 'icon green08',
        html: "<div id='html_icon_" + id + "' class='animated-icon green03 my-icon-id'></div>",
    });  
        
    return icon;
}

var markerList= [];
$( document ).ready(function() {
    var channel = 'Sensors';
    
    var pn = new PubNub({
        //publishKey: 'pub-c-4c63bcfd-ef3d-49ff-a17a-5db83c247fe0',
        subscribeKey: 'sub-c-476b1cce-9904-11e6-bb35-0619f8945a4f',
        ssl: (('https:' == document.location.protocol) ? true : false)
      });
      
    var map = eon.map({
        pubnub: pn,
        id: 'map',
        mbId: 'mapbox.dark',
        mbToken: 'pk.eyJ1IjoibWlrayIsImEiOiJjaXVweDVvaWwwMDBtMnlsOGNsMDU1bWgyIn0.ZrFKwYUj-4oYlTLDXNEtaw',
        channels:[channel],
        connect: function(){
//            var lc = L.control.locate().addTo(map);
//            lc.start();
            map.setView([49.488332, 0.122278], 13);
        },
        transform: function(data){
            var new_data = JSON.parse(JSON.stringify(data));
            for (var i = 0; i < new_data.length; i++) {
                new_data[i] = {
                    latlng: [
                        new_data[i].lt ,
                        new_data[i].lg
                    ],
                    data: {id : new_data[i].id, gasArray: new_data[i].gasArray}
                }
            }
            return new_data;
        },
        message: function (data) {
            $.each(data, function( index, item ) {
                var marker = markerList.find(x => x.options.id === item.data.id)
                if(marker){
                    var maxGasRate = 0;
                    var popUpText = "<div class='popup'>";
                    $.each(item.data.gasArray, function( key, value ) {
                        if(value > maxGasRate){
                            maxGasRate = value;
                        }

                        popUpText = popUpText + "<div class='item'>" + "Taux " + key + " : " + value + "%" + "</div>"
                    })
                    popUpText = popUpText + "</div>"
                    marker.setPopupContent(popUpText)
                    
                    marker.options.icon.setColor(maxGasRate);
                }
            });
        },
        marker: function (latlng, data) {
            
            var icon = createIcon(data.id)
            var marker = new L.Marker(latlng, {
                id: data.id,
                icon: icon,
                opacity: 0.7,
            });

            marker.bindPopup("");

            markerList.push(marker);
            
            icon.animate();
            
            return marker;
          }
      });
});